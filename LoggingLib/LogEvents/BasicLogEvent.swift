import Foundation

struct BasicLogEvent: Codable, LogEvent {
    
    // MARK: - Internal Types
    
    enum CodingKeys: String, CodingKey {
        
        case type
        case unit
        case event
        case message
        case time
        
    }
    
    // MARK: - Internal properties
    
    let type: LogEventType
    
    // MARK: - Private properties
    
    private let unit: String
    private let event: String
    private let message: String
    private let time: String
    
    private let jsonEncoder = JSONEncoder()
    private let dateFormatter = DateFormatter()
    private let dateFormat = "dd.MM.yyyy HH:mm:ss.SSSZ"
    
    // MARK: - Init
    
    init(unit: String, type: LogEventType, event: String, message: String, time: Date) {
        self.unit = unit
        self.type = type
        self.event = event
        self.message = message
        self.dateFormatter.dateFormat = dateFormat
        self.time = dateFormatter.string(from: time)
    }
    
    // MARK: - LogEvent Protocol
    
    func plainLogInfo() -> String {
        var components: [String] = []
        components += ["\(time)"]
        components += ["T:\(Unmanaged.passUnretained(Thread.current).toOpaque())" + (Thread.isMainThread ? "(M)" : "")]
        switch type {
        case .error:
            components += ["\u{001B}[1;31m"]
            
        case .verbose:
            components += ["\u{001B}[0;37m"]
            
        case .info:
            components += ["\u{001B}[0;36m"]
            
        case .warning:
            components += ["\u{001B}[1;33m"]
        }
        components += ["[\(type.rawValue.uppercased())]"]
        components += ["\u{001B}[0m"]
        components += ["[\(unit)]"]
        components += ["<\(event)>:"]
        components += ["\(message)"]
        return components.joined(separator: " ")
    }
    
    func jsonLogInfo() -> String {
        jsonEncoder.outputFormatting = [.prettyPrinted]
        let encoded = (try? jsonEncoder.encode(self)) ?? Data()
        return String(data: encoded, encoding: .utf8) ?? "{ \"logging_error\" : \"can't encode object: \(self)\" }"
    }
    
    func elasticLogInfo() -> String {
        jsonEncoder.outputFormatting = []
        let encoded = (try? jsonEncoder.encode(self)) ?? Data()
        return String(data: encoded, encoding: .utf8) ?? "{ \"logging_error\" : \"can't encode object: \(self)\" }"
    }
    
}
