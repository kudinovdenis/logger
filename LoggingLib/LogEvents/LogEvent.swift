import Foundation

enum LogEventType: String, Codable {
    
    case info
    case error
    case warning
    case verbose
    
}

protocol LogEvent {
    
    var type: LogEventType { get }
    
    func plainLogInfo() -> String 
    func jsonLogInfo() -> String
    func elasticLogInfo() -> String
    
}
