import Foundation

protocol LogWritterDelegate: AnyObject {
    
    func logWritter(logWriter: LogWriter, goingToWriteDataSize: Int)
    
}

class LogWriter {
    
    enum Error: Swift.Error {
        
        case incompatibleTypeOfDestination(Destination)
        case fileDoesNotExists(URL)
        case unknownError
        
    }
    
    enum Mode {
        
        case `default`
        case json
        case jsonl
        
    }
    
    enum Destination {
        
        case console
        case file(URL)
        
    }
    
    private var destinationStream: OutputStream? = nil
    private let writeQueue = DispatchQueue(label: "com.devices_monitor.logging.queue",
                                           qos: .utility,
                                           attributes: [],
                                           autoreleaseFrequency: .inherit,
                                           target: nil)
    weak var delegate: LogWritterDelegate?
    let mode: Mode
    private (set) var destination: Destination
    
    init(destination: Destination, mode: Mode) {
        switch destination {
        case .console:
            break
            
        case .file(let url):
            if !FileManager.default.fileExists(atPath: url.path) {
                FileManager.default.createFile(atPath: url.path, contents: nil, attributes: nil)
            }
            
            guard let stream = OutputStream(toFileAtPath: url.path, append: true) else {
                fatalError("Cant open log file \(url) to write.")
            }
            
            destinationStream = stream
            destinationStream?.open()
        }
        self.destination = destination
        self.mode = mode
    }
    
    func write(logEvent: LogEvent) {
        let logString: String
        switch mode {
        case .default:
            logString = logEvent.plainLogInfo()
            
        case .json:
            logString = logEvent.jsonLogInfo() + ","
            
        case .jsonl:
            logString = "{\"index\" : {}}\n" + logEvent.elasticLogInfo()
        }
        write(string: logString)
    }
    
    func write(string: String) {
        write(data: (string + "\n").data(using: .utf8)!)
    }
    
    func write(data: Data) {
        delegate?.logWritter(logWriter: self, goingToWriteDataSize: data.count)
        writeQueue.sync {
            switch destination {
            case .console:
                FileHandle.standardOutput.write(data)
                
            case .file:
                let nsData = data as NSData
                let bytes = nsData.bytes.assumingMemoryBound(to: UInt8.self)
                let unsafeRawPointer = UnsafePointer(bytes)
                _  = destinationStream?.write(unsafeRawPointer, maxLength: data.count)
            }
        }
    }
    
    func changeDestination(to destination: Destination) {
        // To prevent destination change while write is in progress.
        writeQueue.sync {
            self.destination = destination
            
            switch destination {
            case .console:
                destinationStream?.close()
                self.destination = destination
                
            case .file(let url):
                guard let stream = OutputStream(toFileAtPath: url.path, append: true) else {
                    fatalError("Cant open log file \(url) to write.")
                }
                
                destinationStream?.close()
                
                destinationStream = stream
                destinationStream?.open()
            }
        }
    }
    
    func currentFileSize() throws -> Int {
        switch destination {
        case .console:
            throw Error.incompatibleTypeOfDestination(destination)
            
        case .file(let url):
            guard FileManager.default.fileExists(atPath: url.path) else {
                throw Error.fileDoesNotExists(url)
            }
            
            guard let fileSize = try FileManager.default.attributesOfItem(atPath: url.path)[FileAttributeKey.size] as? Int else {
                throw Error.unknownError
            }
            
            return fileSize
        }
    }
    
}
