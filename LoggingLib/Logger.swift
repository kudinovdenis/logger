import Foundation

public protocol Logger {
    
    func error(event: String, message: String)
    func warning(event: String, message: String)
    func info(event: String, message: String)
    func verbose(event: String, message: String)
    
    func error(event: String, message: String, objects: (String, Codable) ...)
    func warning(event: String, message: String, objects: (String, Codable) ...)
    func info(event: String, message: String, objects: (String, Codable) ...)
    func verbose(event: String, message: String, objects: (String, Codable) ...)
    
}

public struct BasicLogger: Logger {
    
    private let name: String
    
    public func error(event: String, message: String) {
        let logEvent = BasicLogEvent(unit: name, type: .error, event: event, message: message, time: Date())
        LoggingController.shared.log(event: logEvent)
    }
    
    public func warning(event: String, message: String) {
        let logEvent = BasicLogEvent(unit: name, type: .warning, event: event, message: message, time: Date())
        LoggingController.shared.log(event: logEvent)
    }
    
    public func info(event: String, message: String) {
        let logEvent = BasicLogEvent(unit: name, type: .info, event: event, message: message, time: Date())
        LoggingController.shared.log(event: logEvent)
    }
    
    public func verbose(event: String, message: String) {
        let logEvent = BasicLogEvent(unit: name, type: .verbose, event: event, message: message, time: Date())
        LoggingController.shared.log(event: logEvent)
    }
    
    public func error(event: String, message: String, objects: (String, Codable) ...) {
        let wrappedObjects = objects.map { ObjectLogWrapper(name: $0.0, object: $0.1) }
        let logEvent = ObjectLogEvent(unit: name, type: .error, event: event, message: message, time: Date(), objects: wrappedObjects)
        LoggingController.shared.log(event: logEvent)
    }
    
    public func warning(event: String, message: String, objects: (String, Codable) ...) {
        let wrappedObjects = objects.map { ObjectLogWrapper(name: $0.0, object: $0.1) }
        let logEvent = ObjectLogEvent(unit: name, type: .warning, event: event, message: message, time: Date(), objects: wrappedObjects)
        LoggingController.shared.log(event: logEvent)
    }
    
    public func info(event: String, message: String, objects: (String, Codable) ...) {
        let wrappedObjects = objects.map { ObjectLogWrapper(name: $0.0, object: $0.1) }
        let logEvent = ObjectLogEvent(unit: name, type: .info, event: event, message: message, time: Date(), objects: wrappedObjects)
        LoggingController.shared.log(event: logEvent)
    }
    
    public func verbose(event: String, message: String, objects: (String, Codable) ...) {
        let wrappedObjects = objects.map { ObjectLogWrapper(name: $0.0, object: $0.1) }
        let logEvent = ObjectLogEvent(unit: name, type: .verbose, event: event, message: message, time: Date(), objects: wrappedObjects)
        LoggingController.shared.log(event: logEvent)
    }
    
    public init(name: String) {
        self.name = name
    }
    
}
