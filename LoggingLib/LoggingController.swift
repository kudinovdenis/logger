import Foundation

public class LoggingController {
    
    public enum LogLevel: UInt {
        
        case none
        case info
        case verbose
        
    }
    
    typealias Format = LogWriter.Mode
    typealias Destination = LogWriter.Destination
    
    public static let shared = LoggingController(loggers: [(.jsonl, .file(URL(string: "./log-\(UUID().uuidString).jsonl")!)),
                                                           (.default, .console)],
                                                 maximumLogFileSize: 10 * 1024 * 1024) // 10 MB
    
    private let logWriters: [LogWriter]
    private let maximumLogFileSize: Int
    public var logToFileEnabled: Bool = true
    public var logLevel: LogLevel = .none
    
    init(loggers: [(Format, Destination)], maximumLogFileSize: Int) {
        logWriters = loggers.map { LogWriter(destination: $0.1, mode: $0.0) }
        self.maximumLogFileSize = maximumLogFileSize
        logWriters.forEach { $0.delegate = self }
    }
    
    func log(event: LogEvent) {
        guard shouldWriteEvent(event) else {
            return
        }
        
        logWriters.forEach { logWriter in
            if !logToFileEnabled, case .file = logWriter.destination {
                return
            }
            logWriter.write(logEvent: event)
        }
    }
    
    private func shouldWriteEvent(_ event: LogEvent) -> Bool {
        switch event.type {
        case .info,
             .warning,
             .error:
            return logLevel.rawValue > LogLevel.none.rawValue
            
        case .verbose:
            return logLevel.rawValue >= LogLevel.verbose.rawValue
        }
    }
    
}

extension LoggingController: LogWritterDelegate {
    
    func logWritter(logWriter: LogWriter, goingToWriteDataSize: Int) {
        switch logWriter.destination {
        case .console:
            // Nothing to do.
            return
            
        case .file:
            do {
                let currentSize = try logWriter.currentFileSize()
                if currentSize + goingToWriteDataSize > maximumLogFileSize {
                    switch logWriter.mode {
                    case .default:
                        logWriter.changeDestination(to: .file(URL(string: "./log-\(UUID().uuidString).log")!))
                        
                    case .json:
                        logWriter.changeDestination(to: .file(URL(string: "./log-\(UUID().uuidString).json")!))
                        
                    case .jsonl:
                        logWriter.changeDestination(to: .file(URL(string: "./log-\(UUID().uuidString).jsonl")!))
                    }
                }
                else {
                    // Nothing to do.
                    return
                }
            }
            catch {
                print("Log file size control failed with error: \(error)")
            }
        }
    }
    
}
